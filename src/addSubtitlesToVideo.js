// Default config to be used for the subtitles
const defaultSubtitlesConfig = {
    color: "ffffff",
    size: 30,
    fontFamily: "Roboto",
    gravity: "south",
    y: 60
}

/**
 * URL parameter strings must be escape Commas twice to fit the cloudinary API
 * @param {string} str string to escape commas from
 * @returns {string} string with commas escaped
 */
const escapeCommas = (str) => {
    return str.replace(new RegExp(",", "g"), "%2C")
}

/**
 * Encode all the keys of an object with encodeURIComponent
 * @param {object} ob 
 * @returns {object} Object with strings escaped by encodeURIComponent
 */
const URLEncodeObject = (ob) => {
    const res = Object.assign({}, ob)
    Object.keys(res).forEach((k) => {
        if (typeof res[k] === "string") {
            res[k] = encodeURIComponent(escapeCommas(res[k]))
        }
    })
    return res
}

/**
 * Converts subtitles object format to object that fits the time format expected by cloudinary
 * @param {object} subtitle 
 * @returns {object} subtitles with times converted to cloudinary format
 */
const standardizeTime = (subtitle) => {
    const res = Object.assign({}, subtitle)
    res["start-timing"] = timeToSeconds(res["start-timing"])
    res["end-timing"] = timeToSeconds(res["end-timing"])

    return res
}

/**
 * Parse subtitle string time to float
 * @param {string} time 
 * @returns {number} Parsed time as float
 */
const timeToSeconds = (time) => {
    const timeArr = time.split(":")
    return parseFloat(timeArr[0]) * 60 + parseFloat(timeArr[1])
}

/**
 * Parsing a subtitle object to a string that can be used in cloudinary's API
 * @param {object} subtitle Object that contains what text and times to place the subtitle in
 * @param {object} defaultConfig General configuration for subtitle style and placement
 * @returns {string} Containing a single cloudinary URL parameter
 */
const parseSubtitle = (subtitle, defaultConfig=defaultSubtitlesConfig) => {
    const conf = URLEncodeObject(Object.assign({}, defaultSubtitlesConfig, defaultConfig))
    const s = URLEncodeObject(standardizeTime(subtitle))

    const {fontFamily, size, gravity, y, color} = conf
    return `l_text:${fontFamily}_${size}:${s.text},co_rgb:${color},g_${gravity},y_${y},so_${s["start-timing"]},eo_${s["end-timing"]}`
}

/**
 * Applies subtitles to the video specified in videoPublicID
 * @param {string} videoPublicID Public ID of an uploaded cloudinary video
 * @param {object} subtitles Object containing the subtitles to be applied to the video
 * @returns {string} Cloudinary URL with the video subtitles applied
 */
const addSubtitlesToVideo = (videoPublicID, subtitles) => {
    let attributes = ""
    subtitles.subtitles.forEach((s) => {
        attributes += parseSubtitle(s) + "/"
    })

    return `https://res.cloudinary.com/candidate-evaluation/video/upload/${attributes}${videoPublicID}.mp4`
}

export default addSubtitlesToVideo
