# Add Subtitles to Video for Cloudinary's API

## Usage:

```javascript
const addSubtitlesToVideo = require("add-subtitles-to-video")

const subtitles = {
    subtitles:[
        {
        "start-timing":"0:24.8",
        "end-timing":"0:27.2",
        "text":"Hey Sweetie! Sorry I got home so late..."
        },
        {
        "start-timing":"0:27.2",
        "end-timing":"0:30.6",
        "text":"but I had to pick something up after work."
        },{
        "start-timing":"0:30.6",
        "end-timing":"0:34.4",
        "text":"It's such a beautiful day outside, you should let some sun inside."
        },
    ]
}


const url = addSubtitlesToVideo("The_Present", subtitles)
```