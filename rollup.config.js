var rollup = require("rollup");
var babel = require("rollup-plugin-babel");

export default {
    input: 'src/addSubtitlesToVideo.js',
    output: {
      file: 'index.js',
      format: 'umd',
      name: 'subtitlesToVideo',
    },
    plugins: [ babel() ]
  };