const assert = require("assert")
const addSubtitlesToVideo = require("./index")

const subtitles = {
    subtitles:[
        {
        "start-timing":"0:24.8",
        "end-timing":"0:27.2",
        "text":"Hey Sweetie! Sorry I got home so late..."
        },
        {
        "start-timing":"0:27.2",
        "end-timing":"0:30.6",
        "text":"but I had to pick something up after work."
        },{
        "start-timing":"0:30.6",
        "end-timing":"0:34.4",
        "text":"It's such a beautiful day outside, you should let some sun inside."
        },
    ]
}


const url = addSubtitlesToVideo("The_Present", subtitles)

assert.strictEqual(url, "https://res.cloudinary.com/candidate-evaluation/video/upload/l_text:Roboto_30:Hey%20Sweetie!%20Sorry%20I%20got%20home%20so%20late...,co_rgb:ffffff,g_south,y_60,so_24.8,eo_27.2/l_text:Roboto_30:but%20I%20had%20to%20pick%20something%20up%20after%20work.,co_rgb:ffffff,g_south,y_60,so_27.2,eo_30.6/l_text:Roboto_30:It's%20such%20a%20beautiful%20day%20outside%252C%20you%20should%20let%20some%20sun%20inside.,co_rgb:ffffff,g_south,y_60,so_30.6,eo_34.4/The_Present.mp4")

console.log("All tests have passed!")