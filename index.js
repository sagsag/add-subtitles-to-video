(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
    typeof define === 'function' && define.amd ? define(factory) :
    (global = global || self, global.subtitlesToVideo = factory());
}(this, function () { 'use strict';

    // Default config to be used for the subtitles
    var defaultSubtitlesConfig = {
      color: "ffffff",
      size: 30,
      fontFamily: "Roboto",
      gravity: "south",
      y: 60
      /**
       * URL parameter strings must be escape Commas twice to fit the cloudinary API
       * @param {string} str string to escape commas from
       * @returns {string} string with commas escaped
       */

    };

    var escapeCommas = function escapeCommas(str) {
      return str.replace(new RegExp(",", "g"), "%2C");
    };
    /**
     * Encode all the keys of an object with encodeURIComponent
     * @param {object} ob 
     * @returns {object} Object with strings escaped by encodeURIComponent
     */


    var URLEncodeObject = function URLEncodeObject(ob) {
      var res = Object.assign({}, ob);
      Object.keys(res).forEach(function (k) {
        if (typeof res[k] === "string") {
          res[k] = encodeURIComponent(escapeCommas(res[k]));
        }
      });
      return res;
    };
    /**
     * Converts subtitles object format to object that fits the time format expected by cloudinary
     * @param {object} subtitle 
     * @returns {object} subtitles with times converted to cloudinary format
     */


    var standardizeTime = function standardizeTime(subtitle) {
      var res = Object.assign({}, subtitle);
      res["start-timing"] = timeToSeconds(res["start-timing"]);
      res["end-timing"] = timeToSeconds(res["end-timing"]);
      return res;
    };
    /**
     * Parse subtitle string time to float
     * @param {string} time 
     * @returns {number} Parsed time as float
     */


    var timeToSeconds = function timeToSeconds(time) {
      var timeArr = time.split(":");
      return parseFloat(timeArr[0]) * 60 + parseFloat(timeArr[1]);
    };
    /**
     * Parsing a subtitle object to a string that can be used in cloudinary's API
     * @param {object} subtitle Object that contains what text and times to place the subtitle in
     * @param {object} defaultConfig General configuration for subtitle style and placement
     * @returns {string} Containing a single cloudinary URL parameter
     */


    var parseSubtitle = function parseSubtitle(subtitle) {
      var defaultConfig = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : defaultSubtitlesConfig;
      var conf = URLEncodeObject(Object.assign({}, defaultSubtitlesConfig, defaultConfig));
      var s = URLEncodeObject(standardizeTime(subtitle));
      var fontFamily = conf.fontFamily,
          size = conf.size,
          gravity = conf.gravity,
          y = conf.y,
          color = conf.color;
      return "l_text:".concat(fontFamily, "_").concat(size, ":").concat(s.text, ",co_rgb:").concat(color, ",g_").concat(gravity, ",y_").concat(y, ",so_").concat(s["start-timing"], ",eo_").concat(s["end-timing"]);
    };
    /**
     * Applies subtitles to the video specified in videoPublicID
     * @param {string} videoPublicID Public ID of an uploaded cloudinary video
     * @param {object} subtitles Object containing the subtitles to be applied to the video
     * @returns {string} Cloudinary URL with the video subtitles applied
     */


    var addSubtitlesToVideo = function addSubtitlesToVideo(videoPublicID, subtitles) {
      var attributes = "";
      subtitles.subtitles.forEach(function (s) {
        attributes += parseSubtitle(s) + "/";
      });
      return "https://res.cloudinary.com/candidate-evaluation/video/upload/".concat(attributes).concat(videoPublicID, ".mp4");
    };

    return addSubtitlesToVideo;

}));
//# sourceMappingURL=index.js.map
